/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import eu.welive.bb.events.data.DataWrapper;
import eu.welive.bb.events.data.DataWrapperException;
import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.exception.EntityNotFoundException;
import eu.welive.bb.events.exception.EventException;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.EventList;
import eu.welive.bb.events.serialization.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("agenda")
@Api(value = "agenda")
public class AgendaAPI {
	
	private static final String OPERATION_SUCCESSFUL = "Operation successful";
	
	private final DataWrapper dataWrapper;
	
	public AgendaAPI() throws EventException {
		try {
	    	final Properties p = new Properties();
			try (final InputStream in = getClass().getResourceAsStream("/conf.properties")) {
				p.load(in);
			}
			
			final String weliveApi = p.getProperty("weliveapi");
			final String dataset = p.getProperty("dataset");
			final String resource = p.getProperty("resource");
			
			final QMConnector qmConnector = new QMConnector(weliveApi);
			
			dataWrapper = new DataWrapper(dataset, resource, qmConnector);
    	} catch (IOException e) {
    		throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Could not load properties from conf file" + e.getMessage());
    	}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets all the registered events",
		response = EventList.class)
	public EventList getAllEvents() throws EventException {
		try {
			final EventList allEvents = new EventList();
			
			final EventList originalEvents = new EventList(dataWrapper.getOriginalEvents());
			allEvents.add(originalEvents);
			
			final EventList userEvents = new EventList(dataWrapper.getUserEvents());
			allEvents.add(userEvents);
			
			return allEvents;
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds a new event. Authorization required.",
		response = Message.class)
	public Message saveEvent(@HeaderParam("Authorization") @DefaultValue("") String header,
		Event event) throws EventException {
		
		try {			
			dataWrapper.getQMConnector().setAuthHeader(header);
			dataWrapper.saveEvent(event);
			return new Message(OPERATION_SUCCESSFUL);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates an existing event. Authorization required.",
		response = Message.class)
	public Message updateEvent(@HeaderParam("Authorization") @DefaultValue("") String header,
		@PathParam("id") int id,
		Event event) throws EventException {
		
		try {			
			dataWrapper.getQMConnector().setAuthHeader(header);
			dataWrapper.updateEvent(id, event);
			return new Message(OPERATION_SUCCESSFUL);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes an existing event. Authorization required.",
		response = Message.class)
	public Message deleteEvent(@HeaderParam("Authorization") @DefaultValue("") String header,
			@PathParam("id") int id) throws EventException {
		try {
			dataWrapper.getQMConnector().setAuthHeader(header);
			final int updatedRows = dataWrapper.deleteEvent(id);
			if (updatedRows > 0) {
				return new Message(OPERATION_SUCCESSFUL); 
			} else {
				throw new EntityNotFoundException("Could not find event with id: " + id);
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
}
