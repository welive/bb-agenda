/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class Config {

	private static Config instance = null;
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}

	private String apiPath;
	private String weliveAPI;
	private String aacServiceURL;
	private String clientId;
	
	private URL baseURL;
	private String homePath;
	
	private Config() {
		final Properties p = new Properties();
		try (final InputStream in = Config.class.getResourceAsStream("/conf.properties")) {
			p.load(in);
			
			apiPath = p.getProperty("apipath");
			weliveAPI = p.getProperty("weliveapi");
			aacServiceURL = p.getProperty("aacserviceurl");
			clientId = p.getProperty("clientid");
			baseURL = new URL(p.getProperty("baseurl"));
			homePath = p.getProperty("homepath");
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	public String getWeLiveAPI() {
		return weliveAPI;
	}

	public String getAacServiceURL() {
		return aacServiceURL;
	}

	public String getClientId() {
		return clientId;
	}
	
	public String getHost() {
		return baseURL.getHost() + ((baseURL.getPort() == -1)?"":(":" + baseURL.getPort()));
	}
	
	public String getHomeURL() {
		return baseURL + homePath;
	}
	
	public String getSwaggerPath() {
		return baseURL.getPath() + apiPath;
	}
}
