/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.data.qm.QMConnectorException;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Legend;

public class DataWrapper {
	
	final static Logger logger = Logger.getLogger(DataWrapper.class);
	
	enum Origin { ORIGIN, USER };
	
	private final QMConnector qmConnector;
	
	private final String dataset;
	private final String resource;
	
	public DataWrapper(String dataset, String resource, QMConnector qmConnector) {
		this.dataset = dataset;
		this.resource = resource;
		this.qmConnector = qmConnector;
	}
	
	public QMConnector getQMConnector() {
		return qmConnector;
	}
	
	public List<Event> getOriginalEvents() throws DataWrapperException {
		logger.debug("Getting all original events");
		try {
			String query = "SELECT rootTable.id, rootTable._id, rootTable.tipo, lugar, fecha_desde, fecha_hasta, hora, info, titulo, " + 
					"direccion, observaciones, distrito FROM rootTable, rootTable_distritos " +
					"WHERE rootTable_distritos.parent_id == rootTable._id";
			
			logger.debug("Executing query:");
			logger.debug(query);
			
			final Map<String, String> queryParams = new HashMap<String, String>();
			queryParams.put("origin", "ORIGIN");
			
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource, queryParams);			
			final Map<Integer, Event> partialEvents = processEventList(result);
			
			final Map<Integer, Legend> legends = getLegends(partialEvents, Origin.ORIGIN);
			final List<Event> events = merge(partialEvents, legends);
			
			return events;			
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}

	public List<Event> getUserEvents() throws DataWrapperException {
		logger.debug("Getting all user events");
		try {
			String query = "SELECT DISTINCT(rootTable.id), rootTable._id, rootTable.tipo, lugar, fecha_desde, fecha_hasta, hora, info, titulo, " + 
					"direccion, observaciones, distrito, rootTable._row_user_ as user " +
					"FROM rootTable, rootTable_distritos " +
					"WHERE rootTable_distritos.parent_id == rootTable._id";
			
			logger.debug("Executing query:");
			logger.debug(query);
			
			final Map<String, String> queryParams = new HashMap<String, String>();
			queryParams.put("origin", "USER");
			
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource, queryParams);			
			final Map<Integer, Event> partialEvents = processEventList(result);
			
			final Map<Integer, Legend> legends = getLegends(partialEvents, Origin.USER);
			final List<Event> events = merge(partialEvents, legends);
			
			return events;			
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public Map<Integer, Event> processEventList(JsonObject data) {
		final Map<Integer, Event> events = new HashMap<Integer, Event>();
		
		final JsonArray values = data.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final Event event = processEvent(json);
			events.put(json.getInt("_id"), event);
		}
		
		return events;
	}

	private String getValue(JsonObject json, String key) {
		if (json.containsKey(key)) {
			return json.getString(key);
		} else {
			return "";
		}
	}
	
	private String getId(JsonObject json) {
		if (json.containsKey("user")) {
			return Integer.toString(json.getInt("_id"));
		} else {
			return json.getString("id");
		}
	}
	
	private Map<Integer, Legend> getLegends(Map<Integer, Event> events, Origin origin) throws QMConnectorException {		
		String query = "SELECT _id, tipo, idioma, parent_id from rootTable_leyenda WHERE parent_id in";
		final StringJoiner joiner = new StringJoiner(",", "(", ")");
		for (final Integer id : events.keySet()) {
			joiner.add(id.toString());
		}
		query += joiner.toString();
		
		final Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("origin", origin.toString());
		
		final JsonObject result = qmConnector.queryDataset(query, dataset, resource, queryParams);
		
		final Map<Integer, Legend> legends = new HashMap<Integer, Legend>();
		final JsonArray values = result.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final int parentId = Integer.parseInt(json.getString("parent_id"));
			final String access = getValue(json, "tipo");
			final String language = getValue(json, "idioma");
			
			Legend legend; 
			if (legends.containsKey(parentId)) {
				legend = legends.get(parentId);
			} else {
				legend = new Legend();
			}
			
			if (!access.isEmpty()) {
				legend.setAccess(access);
			}
			
			if (!language.isEmpty()) {
				legend.setLanguage(language);
			}
			
			legends.put(parentId, legend);
		}
		
		return legends;
	}
	
	private List<Event> merge(Map<Integer, Event> eventMap, Map<Integer, Legend> legends) {
		final List<Event> events = new ArrayList<Event>();
		for (Entry<Integer, Event> entry : eventMap.entrySet()) {
			final Legend legend = legends.get(entry.getKey());
			
			final Event event = entry.getValue();
			event.setAccess(legend.getAccess());
			event.setLang(legend.getLanguage());
			
			events.add(event);
		}
		return events;
	}
	
	private Event processEvent(JsonObject json) {
		final Event event = new Event();
		event.setId(getId(json));
		event.setType(getValue(json,"tipo"));
		event.setPlace(getValue(json,"lugar"));
		event.setInfo(getValue(json,"info"));
		event.setTitle(getValue(json,"titulo"));
		event.setAddress(getValue(json,"direccion"));
		event.setObservations(getValue(json,"observaciones"));
		event.setStartDate(getValue(json, "fecha_desde"));
		event.setEndDate(getValue(json, "fecha_hasta"));
		event.setHour(getValue(json, "hora"));
		event.setDistrict(getValue(json,"distrito"));
		event.setUser(getValue(json, "user"));
		
		return event;
	}
	
	public int saveEvent(Event event) throws DataWrapperException {
		logger.debug("Saving event: " + event);
		
		final List<String> inserts = new ArrayList<String>();
		
		final String insertRootTableTemplate = "INSERT INTO rootTable (_id, tipo, lugar, info, titulo, direccion, fecha_desde, fecha_hasta, hora, observaciones) VALUES (null, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
		final String insertRootTable = String.format(insertRootTableTemplate,event.getType(), 
			StringEscapeUtils.escapeSql(event.getPlace()), 
			StringEscapeUtils.escapeSql(event.getInfo()), 
			StringEscapeUtils.escapeSql(event.getTitle()), 
			StringEscapeUtils.escapeSql(event.getAddress()),
			StringEscapeUtils.escapeSql(event.getStartDate()),
			StringEscapeUtils.escapeSql(event.getEndDate()),
			StringEscapeUtils.escapeSql(event.getHour()),
			StringEscapeUtils.escapeSql(event.getObservations()));
		inserts.add(insertRootTable);
		
		final String insertLeyendaTemplate = "INSERT INTO rootTable_leyenda (_id, parent_id, tipo, idioma) SELECT MAX(_id), MAX(_id), '%s', '%s' FROM rootTable";
		final String insertLeyenda = String.format(insertLeyendaTemplate, 
			StringEscapeUtils.escapeSql(event.getAccess()), 
			StringEscapeUtils.escapeSql(event.getLang()));
		inserts.add(insertLeyenda);
		
		final String insertDistritoTemplate = "INSERT INTO rootTable_distritos (_id, parent_id, distrito) SELECT MAX(_id), MAX(_id), '%s' FROM rootTable";
		final String insertDistrito = String.format(insertDistritoTemplate, 
				StringEscapeUtils.escapeSql(event.getDistrict()));
		inserts.add(insertDistrito);
		
		logger.debug("Executing inserts: ");
		logger.debug(inserts);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(inserts, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int updateEvent(int id, Event event) throws DataWrapperException {
		logger.debug("Updating event: " + event);
		
		final List<String> updates = new ArrayList<String>();
		
		final String updateRootTableTemplate = "UPDATE rootTable SET tipo='%s', lugar='%s', info='%s', titulo='%s', direccion='%s', fecha_desde='%s', fecha_hasta='%s, hora='%s', observaciones='%s' WHERE _id=%d";
		final String updateRootable = String.format(updateRootTableTemplate, 
				StringEscapeUtils.escapeSql(event.getType()), 
				StringEscapeUtils.escapeSql(event.getPlace()), 
				StringEscapeUtils.escapeSql(event.getInfo()), 
				StringEscapeUtils.escapeSql(event.getTitle()), 
				StringEscapeUtils.escapeSql(event.getAddress()),
				StringEscapeUtils.escapeSql(event.getStartDate()),
				StringEscapeUtils.escapeSql(event.getEndDate()),
				StringEscapeUtils.escapeSql(event.getHour()), 
				StringEscapeUtils.escapeSql(event.getObservations()), id);
		updates.add(updateRootable);
		
		final String updateLeyendaTemplate = "UPDATE rootTable_leyenda SET tipo='%s', idioma='%s' WHERE parent_id='%d'";
		final String updateLeyenda = String.format(updateLeyendaTemplate, 
			StringEscapeUtils.escapeSql(event.getAccess()), 
			StringEscapeUtils.escapeSql(event.getLang()), 
			id);
		updates.add(updateLeyenda);
		
		final String updateDistritoTemplate = "UPDATE rootTable_distritos SET distrito='%s' WHERE parent_id='%d'";
		final String updateDistrito = String.format(updateDistritoTemplate, 
			StringEscapeUtils.escapeSql(event.getDistrict()), 
			id);
		updates.add(updateDistrito);
		
		logger.debug("Executing updates: ");
		logger.debug(updates);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(updates, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int deleteEvent(int eventId) throws DataWrapperException {
		logger.debug("Deleting event with eventId: " + eventId);
		
		final List<String> deletes = new ArrayList<String>();
		
		final String deleteRootTableTemplate = "DELETE FROM rootTable WHERE _id=%d";
		final String deleteRootTable = String.format(deleteRootTableTemplate, eventId);
		deletes.add(deleteRootTable);
		
		final String deleteLeyendaTemplate = "DELETE FROM rootTable_leyenda WHERE parent_id='%d'";
		final String deleteLeyenda = String.format(deleteLeyendaTemplate, eventId); 
		deletes.add(deleteLeyenda);		
		
		final String deleteDistritosTemplate = "DELETE FROM rootTable_distritos WHERE parent_id='%d'";
		final String deleteDistritos = String.format(deleteDistritosTemplate, eventId); 
		deletes.add(deleteDistritos);
		
		logger.debug("Executing deletes:");
		logger.debug(deletes);

		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(deletes, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
}
