/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.data;

import javax.ws.rs.core.Response.Status;

@SuppressWarnings("serial")
public class DataWrapperException extends Exception {
	
	private final String reason;
	private final int status;
	
	public DataWrapperException(String message) {
		super(message);
		this.reason = "";
		this.status = Status.INTERNAL_SERVER_ERROR.getStatusCode();
	} 
	
	public String getReason() {
		return reason;
	}
	
	public int getStatus() {
		return status;
	}
}
