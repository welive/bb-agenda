/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

import java.util.ArrayList;
import java.util.List;

public class EventList {
	
	private List<Event> events;
	
	public EventList() {
		this.events = new ArrayList<Event>();
	}
	
	public EventList(List<Event> events) {
		this.events = events;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public void add(EventList eventList) {
		this.events.addAll(eventList.getEvents());		
	}

	@Override
	public String toString() {
		return "EventList [events=" + events + "]";
	}
	
}
