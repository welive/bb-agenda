function getEventID(url) {
  var startIndex = url.indexOf("event_id=");
  if (startIndex > -1) {
    var event_id = url.substring(startIndex + "event_id=".length, url.length);
    return event_id
  } else {
    return 0;
  }
}

function saveEventData(eventId, event, token) {
  var postURL = "api/event";

  $.ajax({
    type: "POST",
    url: postURL,
    headers: {
        'Authorization': 'Bearer ' + token
    },
    data: JSON.stringify(event),
    contentType: "application/json",
    success: function() {
      changesSaved();
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function saveEventTranslation(eventId, event, lang, token) {
  var postURL = "api/event/" + eventId + "/translation/" + lang;

  $.ajax({
    type: "POST",
    url: postURL,
    headers: {
        'Authorization': 'Bearer ' + token
    },
    data: JSON.stringify(event),
    contentType: "application/json",
    success: function() {
      changesSaved();
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function updateEventData(eventId, event, token) {
  var putURL = "api/event/" + eventId;

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + token
    },
    data: JSON.stringify(event),
    contentType: "application/json",
    success: function() {
      changesSaved();
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function updateEventTranslation(eventId, event, lang, token) {
  var putURL = "api/event/" + eventId + "/translation/" + lang;

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + token
    },
    data: JSON.stringify(event),
    contentType: "application/json",
    success: function() {
      changesSaved();
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function editButton(eventId) {
  location.href = 'editEvent?event_id=' + eventId;
}

function listAttendants(eventId) {
  location.href = 'listAttendants?event_id=' + eventId;
}

function langClick(e) {
  location.href = 'editEvent?event_id=' + $("#id").text() + '&lang=' + $(e).text();
}

function checkinUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/checkin";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function checkoutUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/checkout";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function clickCheckBox(eventId, userId) {
  if ($("#checkBox-" + userId).prop("checked")) {
    checkinUser(eventId, userId);
  } else {
    checkoutUser(eventId, userId);
  }
}

function pendingChanges() {
  $("#message").removeClass("green");
  $("#message").addClass("red");
  $("#saveButton").prop("disabled", false);
  $("#message").text("Hay cambios pendientes de guardar");
}

function changesSaved() {
  $("#saveButton").prop("disabled", true);
  $("#message").removeClass("red");
  $("#message").addClass("green");
  $("#message").text("Cambios guardados correctamente");
}

function errorMessage(message) {
  $("#message").removeClass("red");
  $("#message").text(message);
}
