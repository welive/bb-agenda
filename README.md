Bilbao Agenda BB
================

This project contains the Bilbao Agenda building block.

Requirements
-------------

The project has the following requirements that must be correctly installed and
configured:

*	Java SE Development Kit 7. Available at [http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
* 	Maven 3. Available at [http://maven.apache.org/](http://maven.apache.org/)

Installation
------------

Configure the file conf.properties located inside the src/main/resources with the required information.
Create the war file:

	mvn package